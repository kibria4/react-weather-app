var React = require('react');

var About = (props) => {
  return (
    <div>
      <h1 className="page-title text-center">About</h1>
      <p>
        This is my first React app, building a weather app, which uses an API to
        fetch weather data based on user input.
      </p>
      <p>
        Here are some of the tools I used:
      </p>
      <ul>
        <li>
          <a href="https://facebook.github.io/react">React</a> - This was the
            JavaScript framework used.
        </li>
        <li>
          <a href="http://openweathermap.org">Open Weather Map</a> - I used Open
            Weather Map to search for weather data by city name.
        </li>
      </ul>
    </div>
  );
}

module.exports = About;
